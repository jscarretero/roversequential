public class Plateau
{
	private int width = 0;
	private int height = 0;
	
	public Plateau (int width, int height) {
		this.width = width;
		this.height = height;
		checkDimensions();
		//System.out.println("Creating Pleateau " + width + " " + height);
	}
	
	private void checkDimensions() {
		if ((width < 0) || (height < 0)) {
			throw new IllegalArgumentException("Wrong plateau dimensions");
		}
	}
	
	public int width()  {return width;}
	public int height() {return height;}		
}
