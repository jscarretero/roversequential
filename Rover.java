public class Rover
{	
	private static int numRovers = 0;
	private int roverId = 0;
	private int xCoord = 0;
	private int yCoord = 0;
	private Direction direction = Direction.N;
	private String commands;
	private Plateau plat;
	
	public Rover (int x, int y, Direction dir, Plateau plat, String comm) {
		this.xCoord = x;
		this.yCoord = y;
		this.direction = dir;
		this.roverId = numRovers++;
		this.commands = comm;
		this.plat = plat;
		//System.out.println("Creating ROVER " + x + " " + y + " " + dir.stringDir() + " " + commands);
		checkRoverBoundaries();		
	}

	public void processCommands() {
		for (int i = 0; i < commands.length(); ++i) {
			char command = commands.charAt(i);
			process(command);
		}
	}
	
	public void process (char command) {
		switch (command) {
			case 'L':   turnLeft();	 break;
			case 'R':   turnRight(); break;
			case 'M':	move();   	 break;
			default:    throw new IllegalArgumentException("Wrong command received by rover " + Integer.toString(roverId));
		}
	}
		
	private void turnLeft() {
		// Not efficient, could use integer value as direction and decrement plus modulo 
		// to rotate anti-clockwise. 
		switch (direction) {
			case N: direction = Direction.W; break;
			case E: direction = Direction.N; break;
			case S: direction = Direction.E; break;
			case W: direction = Direction.S; break;			
		}
	}
	
	private void turnRight() {
		// Not efficient, could use integer value as direction and increment plus modulo 
		// to rotate clockwise. 
		switch (direction) {
			case N: direction = Direction.E; break;
			case E: direction = Direction.S; break;
			case S: direction = Direction.W; break;
			case W: direction = Direction.N; break;						
		}
	}
	
	private void move () {
		// Not checking if another Rover is in that coordinate.
		switch (direction) {
			case N:  yCoord++;  break;
			case E:  xCoord++;  break;
			case S:  yCoord--;  break;
			case W:  xCoord--; break;
			default: break; // not possible to jump here					
		}
		// Also, not checking if Rover is out of boundaries.
		checkRoverBoundaries();
	}
	
	private void checkRoverBoundaries() { 
		//if ((xCoord < 0) || (yCoord < 0) || (xCoord >= plat.width()) || (yCoord >= plat.height())) {
		//	throw new IllegalArgumentException("Rover " + Integer.toString(roverId) + " is out of the boundaries");
		//}		
		
		// todo: what is the purpose of reading the pleateau information if  rovers can move outside the boundaries?
		//       (referring to second test case)
		
	}

	public void printPosition() {
		System.out.println("" + Integer.toString(xCoord) + " " + Integer.toString(yCoord) + " " + direction.stringDir());
	}
	
}