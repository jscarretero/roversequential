import java.util.Scanner;

public class RoverMain {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
				
		String plateauData;
		String roverPos;
		String roverCommands;
		Plateau myPlateau;
		Rover myRover;
				
		//First read Plateau info.
		plateauData = scanner.nextLine();
		String[] plateauInfo = plateauData.split(" ");
		checkPlateauInfo(plateauInfo);
		myPlateau = new Plateau(Integer.parseInt(plateauInfo[0]), Integer.parseInt(plateauInfo[1]));
		// The plateau information (and class) are useless because rovers can move out of the boundaries
		// Implementing it just in case for a version where rovers would need to check boundaries
				
		//Then read Rover info
		while ( (roverPos = scanner.nextLine()) != null && (roverPos.length() != 0)) {
			try 
			{
				String[] roverInfo = roverPos.split(" ");				
				checkRoverInfo(roverInfo);
				
				int x = Integer.parseInt(roverInfo[0]);
				int y = Integer.parseInt(roverInfo[1]);
				Direction dir = Direction.N;				
				
				char d = roverInfo[2].charAt(0);								
				switch (d) {
					case 'N': dir = Direction.N; break; 
					case 'E': dir = Direction.E; break;
					case 'S': dir = Direction.S; break;
					case 'W': dir = Direction.W; break;
					default: /*Already checked*/ break; 
				}
				
				roverCommands = scanner.nextLine();
				myRover = new Rover(x, y, dir, myPlateau, roverCommands); //garbage collector will easily take care
				myRover.processCommands();	
				myRover.printPosition();
			} catch (Exception e) {
				System.out.println("Exiting.");
				System.exit(1);
			}
		}
		
		scanner.close();
		System.exit(0);
		
	}	
	
	private static void checkPlateauInfo(String[] plateauInfo) {
		// Would check for: 2 strings in plateauInfo
		// Both are positive integers
	}
	private static void checkRoverInfo(String[] roverInfo) {
		// Would check for: 3 strings in roverInfo
		// First two are positive integers
		// Last is a valid direction 
	}	
}





